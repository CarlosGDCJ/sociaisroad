package br.com.example.android.sociaisroad.network.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;
import br.com.example.android.sociaisroad.network.service.AcaoSocialService;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Carlos on 17/12/2017.
 */

public class AcaoSocialApi {
    private static AcaoSocialApi instance;
    private AcaoSocialService acaoSocialService;

    public static AcaoSocialApi getInstance() {
        if (instance == null) {
            instance = new AcaoSocialApi();
        }

        return instance;
    }

    private AcaoSocialApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com/s/50vmlj7dhfaibpj/")
                .addConverterFactory(defaultConverterFactory())
                .build();

        this.acaoSocialService = retrofit.create(AcaoSocialService.class);
    }

    private Converter.Factory defaultConverterFactory() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return GsonConverterFactory.create(gson);
    }

    public Call<AcaoSocialListEntity> getAcoesSociais() {
        return acaoSocialService.getAcoesSociais();
    }

    public Call<AcaoSocialEntity> getAcaoSocialDetail() {
        return acaoSocialService.getAcaoSocialDetail();
    }

    public Call<AcaoSocialListEntity> testaConexao() {
        return acaoSocialService.getAcoesSociais();
    }
}
