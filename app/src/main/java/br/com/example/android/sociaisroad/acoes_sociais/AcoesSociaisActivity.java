package br.com.example.android.sociaisroad.acoes_sociais;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import br.com.example.android.sociaisroad.R;
import br.com.example.android.sociaisroad.acao_social_detail.AcaoSocialDetailActivity;
import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AcoesSociaisActivity extends AppCompatActivity implements AcoesSociaisView {

    @BindView(R.id.rv_acoes_sociais)
    RecyclerView rv_acoes_sociais;
    @BindView(R.id.linear_layout_loading )
    LinearLayout loadingLayout;

    AcoesSociaisPresenter acoesSociaisPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acoes_sociais);
        ButterKnife.bind(this);

        acoesSociaisPresenter = new AcoesSociaisPresenter(this);

        String jsonAcoesSociais = getIntent().getStringExtra("jsonAcoesSociais");
        //atualiza lista passando os dados salvos
        acoesSociaisPresenter.updateList(jsonAcoesSociais);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //infla o menu
        getMenuInflater().inflate(R.menu.menu_download,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //configura o clique no menu
        switch (item.getItemId()) {
            case R.id.action_download:
                acoesSociaisPresenter.saveAcoes(); //salva os dados

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void updateList(final List<AcaoSocialEntity> acoesSociaisList) {

        //seta o adapter
        AcoesSociaisAdapter acoesSociaisAdapter = new AcoesSociaisAdapter(acoesSociaisList, this);
        acoesSociaisAdapter.setOnRecyclerViewSelected(new OnRecyclerViewSelected() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent
                        (AcoesSociaisActivity.this,
                                AcaoSocialDetailActivity.class);

                intent.putExtra("description", acoesSociaisList.get(position).getDescription());
                intent.putExtra("image", acoesSociaisList.get(position).getImage());
                intent.putExtra("site", acoesSociaisList.get(position).getSite());
                intent.putExtra("name", acoesSociaisList.get(position).getName());
                //inicia os details passando como parametro a descrição, o nome. o site e a imagem
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rv_acoes_sociais.setAdapter(acoesSociaisAdapter);
        // criação do gerenciador de layouts
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        rv_acoes_sociais.setLayoutManager(layoutManager);
        rv_acoes_sociais.addItemDecoration(dividerItemDecoration); //coloca a linha de divisão


    }


    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
    } //mostra o loading

    @Override
    public void hideLoading() {
        loadingLayout.setVisibility(View.GONE);
    }//esconde o loading

    @Override
    public void saveMovieSharedPreference(String jsonAcoesSociaisEntity) {
        //salva as informações no app
        SharedPreferences.Editor editor = getSharedPreferences("acoes_sociais_json", MODE_PRIVATE).edit();
        editor.putString("acoes_sociais_entity_json", jsonAcoesSociaisEntity);
        editor.apply();
        showMessage("Informações salvas com sucesso");
    }
}
