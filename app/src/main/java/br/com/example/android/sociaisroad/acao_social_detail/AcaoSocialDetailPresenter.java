package br.com.example.android.sociaisroad.acao_social_detail;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.network.api.AcaoSocialApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Carlos on 17/12/2017.
 */

public class AcaoSocialDetailPresenter {
    public AcaoSocialDetailPresenter(AcaoSocialDetailView acaoSocialDetailView)
    {
        this.acaoSocialDetailView = acaoSocialDetailView;
    }

    AcaoSocialDetailView acaoSocialDetailView;
    private AcaoSocialEntity acaoSocialEntity;

    public void getAcaoSocialDetails(final String description, final String image,  final String site, final String name)
    {
        acaoSocialDetailView.showLoading();
        acaoSocialDetailView.showDetails(description, image, site, name);
        acaoSocialDetailView.hideLoading();
    }
}
