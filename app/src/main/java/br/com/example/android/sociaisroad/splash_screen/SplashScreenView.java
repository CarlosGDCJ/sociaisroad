package br.com.example.android.sociaisroad.splash_screen;

/**
 * Created by Carlos on 17/12/2017.
 */

public interface SplashScreenView {
    void openAcoesSociais();

    void workOffline();

    void openAcoesSociais(String jsonAcoesSociais);
}
