package br.com.example.android.sociaisroad.acoes_sociais;

/**
 * Created by Carlos on 17/12/2017.
 */

import android.view.View;

public interface OnRecyclerViewSelected {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
