package br.com.example.android.sociaisroad.entity;

/**
 * Created by Carlos on 17/12/2017.
 */

public class AcaoSocialEntity {
    private long id;
    private String name;
    private String image;
    private String description;
    private String site;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public String getSite() {
        return site;
    }
}
