package br.com.example.android.sociaisroad.acao_social_detail;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;

/**
 * Created by Carlos on 17/12/2017.
 */

public interface AcaoSocialDetailView {
    void showDetails(String description, String image, String site, String name);
    void showMessage(String msg);
    void showLoading();
    void hideLoading();
}
