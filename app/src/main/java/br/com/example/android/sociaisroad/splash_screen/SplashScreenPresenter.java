package br.com.example.android.sociaisroad.splash_screen;

import android.content.Intent;
import android.os.Handler;

import br.com.example.android.sociaisroad.acoes_sociais.AcoesSociaisActivity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;
import br.com.example.android.sociaisroad.network.api.AcaoSocialApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Carlos on 17/12/2017.
 */

public class SplashScreenPresenter {
    SplashScreenView splashScreenView;

    public SplashScreenPresenter(SplashScreenView splashScreenView) {
        this.splashScreenView = splashScreenView;
    }
    public void entraNoApp() {
        //handler para colocar o delay
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final AcaoSocialApi acaoSocialApi = AcaoSocialApi.getInstance();
                //testa para ver se o app está conectado à internet
                acaoSocialApi.testaConexao()
                        .enqueue(new Callback<AcaoSocialListEntity>() {
                            @Override
                            public void onResponse(Call<AcaoSocialListEntity> call, Response<AcaoSocialListEntity> response) {
                                //se a call for bem sucedida, o app está conectado
                                splashScreenView.openAcoesSociais();
                            }

                            @Override
                            public void onFailure(Call<AcaoSocialListEntity> call, Throwable t) {
                                //se não, é chamada a função de abrir o app offline
                                splashScreenView.workOffline();
                            }
                        });


            }
        },2000);
    }

    public void openAcoesSociais(String jsonAcoesSociais) {
        if (jsonAcoesSociais != null)
        {
            splashScreenView.openAcoesSociais(jsonAcoesSociais);
        }
        else
        {
            splashScreenView.openAcoesSociais();
        }
    }
}
