package br.com.example.android.sociaisroad.splash_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.example.android.sociaisroad.R;
import br.com.example.android.sociaisroad.acoes_sociais.AcoesSociaisActivity;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView {

    SplashScreenPresenter splashScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splashScreenPresenter = new SplashScreenPresenter(this);

        //A splash screen chama a função de entrar no app do presenter
        splashScreenPresenter.entraNoApp();
    }

    public void openAcoesSociais() {
        //abre a activity das ações sociais sem passar nenhum json
        Intent intent = new Intent(SplashScreenActivity.this, AcoesSociaisActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void workOffline() {
        //funçao que busca dados já salvos
        SharedPreferences preferences =
                getSharedPreferences(
                        "acoes_sociais_json",
                        MODE_PRIVATE);
        String jsonAcoesSociais =
                preferences.getString(
                        "acoes_sociais_entity_json",
                        null);
        splashScreenPresenter.openAcoesSociais(jsonAcoesSociais);

    }

    @Override
    public void openAcoesSociais(String jsonAcoesSociais) {
        //abre a activity das ações sociais passando o json salvo
        Intent intent = new Intent(SplashScreenActivity.this, AcoesSociaisActivity.class);
        intent.putExtra("jsonAcoesSociais", jsonAcoesSociais);
        startActivity(intent);
        finish();
    }
}
