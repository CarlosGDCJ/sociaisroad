package br.com.example.android.sociaisroad.acao_social_detail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import br.com.example.android.sociaisroad.R;
import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AcaoSocialDetailActivity extends AppCompatActivity implements AcaoSocialDetailView {

    @BindView(R.id.image_view_header)
    ImageView imgHeader;

    @BindView(R.id.text_view_description)
    TextView Description;

    @BindView(R.id.site)
    TextView Site;

    @BindView(R.id.linear_layout_loading)
    LinearLayout loadingLayout;

    AcaoSocialDetailPresenter acaoSocialDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acao_social_detail);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        String description = intent.getStringExtra("description");
        String image = intent.getStringExtra("image");
        String site = intent.getStringExtra("site");
        String name = intent.getStringExtra("name");

        acaoSocialDetailPresenter = new AcaoSocialDetailPresenter(this);
        //chama a função do presenter que mostra os detalhes
        acaoSocialDetailPresenter.getAcaoSocialDetails(description, image, site, name);
    }

    @Override
    public void showDetails(String description, String image, String site, String name) {
        //mostra os detalhes
        Description.setText(description);
        Site.setText(site);
        Picasso.with(this)
                .load(image)
                .centerCrop()
                .fit()
                .into(imgHeader);
        setTitle(name);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingLayout.setVisibility(View.GONE);
    }
}
