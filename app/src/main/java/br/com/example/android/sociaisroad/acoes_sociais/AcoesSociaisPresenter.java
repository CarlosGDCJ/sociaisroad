package br.com.example.android.sociaisroad.acoes_sociais;

import android.support.annotation.NonNull;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;
import br.com.example.android.sociaisroad.network.api.AcaoSocialApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import android.view.SurfaceHolder.Callback;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Callback;

/**
 * Created by Carlos on 17/12/2017.
 */

public class AcoesSociaisPresenter {

    AcoesSociaisView acoesSociaisView;
    private List<AcaoSocialEntity> acoesList = new ArrayList<>();

    AcaoSocialListEntity acaoSocialListEntity;

    AcoesSociaisPresenter(AcoesSociaisView acoesSociaisView){
        this.acoesSociaisView = acoesSociaisView;
    }

    void updateList(String jsonAcoesSociais){
        if (jsonAcoesSociais != null)
        {
            //faz updade da lista com as informações salvas
            acaoSocialListEntity = new Gson().fromJson(jsonAcoesSociais,AcaoSocialListEntity.class);
            acoesList = acaoSocialListEntity.getAcoesSociais();
            acoesSociaisView.updateList(acoesList);
        }
        else
        {
            //faz o acesso à web para pegar as informações se o json passado for nulo
            final AcaoSocialApi acaoSocialApi = AcaoSocialApi.getInstance();
            acoesSociaisView.showLoading();
            acaoSocialApi.getAcoesSociais().enqueue(new Callback<AcaoSocialListEntity>() {
                @Override
                public void onResponse(Call<AcaoSocialListEntity> call, Response<AcaoSocialListEntity> response) {
                    acaoSocialListEntity = response.body();
                    if(acaoSocialListEntity != null){
                        acoesSociaisView.updateList(acaoSocialListEntity.getAcoesSociais());
                    } else{
                        acoesSociaisView.showMessage("Falha ao carregar informações");
                    }
                    acoesSociaisView.hideLoading();
                }

                @Override
                public void onFailure(Call<AcaoSocialListEntity> call, Throwable t) {
                    acoesSociaisView.hideLoading();
                    acoesSociaisView.showMessage("Falha no acesso ao servidor");
                }

            });
        }


    }

    public void saveAcoes()
    {
        //verifica se há informações a serem salavas e, se houver, salva
        String jsonAcoesSociaisEntity = new Gson().toJson(acaoSocialListEntity);
        //acoesSociaisView.showMessage(jsonAcoesSociaisEntity);
        if (!jsonAcoesSociaisEntity.equals("null"))
        {
            acoesSociaisView.saveMovieSharedPreference(jsonAcoesSociaisEntity);
        }
        else
        {
            acoesSociaisView.showMessage("Não há informações a serem salvas");
        }
    }
}
