package br.com.example.android.sociaisroad.network.service;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Carlos on 17/12/2017.
 */

public interface AcaoSocialService {
    @GET("sociais")
    Call<AcaoSocialListEntity> getAcoesSociais();

    @GET("sociais")
    Call<AcaoSocialEntity> getAcaoSocialDetail();
}
