package br.com.example.android.sociaisroad.acoes_sociais;

import java.util.List;

import br.com.example.android.sociaisroad.entity.AcaoSocialEntity;
import br.com.example.android.sociaisroad.entity.AcaoSocialListEntity;

/**
 * Created by Carlos on 17/12/2017.
 */

public interface AcoesSociaisView {
    void updateList(List<AcaoSocialEntity> acaoSocialList);
    void showMessage(String msg);
    void showLoading();
    void hideLoading();

    void saveMovieSharedPreference(String jsonAcoesSociaisEntity);
}
